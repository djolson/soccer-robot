$fn = 100;

part = "all";  //choose plate [plate1:plate2] || all

cutAreaX = 494;
cutAreaY = 278;

robotR = 154;
screwHoleR = 1.778;
ballR = 110;

motorMountScrewWidth = 1.061*25.4;
motorMountDepth = 0.375*25.4;

wheelR = 2*25.4;
omniWheelR = 25.4;

module motorControllerHoles(){
    translate([90, -45, 0])
        rotate(60){
            translate([22, 22, 0])
                circle(screwHoleR, true);
            translate([32, 12, 0])
                circle(screwHoleR, true);
    
            translate([-22, 22, 0])
                circle(screwHoleR, true);
            translate([-32, 12, 0])
                circle(screwHoleR, true);
    
            translate([22, -22, 0])
                circle(screwHoleR, true);
            translate([32, -12, 0])
                circle(screwHoleR, true);
        
            translate([-22, -22, 0])
                circle(screwHoleR, true);
            translate([-32, -12, 0])
                circle(screwHoleR, true);
        }
    translate([-90, -45, 0])
        rotate(-60){
            translate([22, 22, 0])
                circle(screwHoleR, true);
            translate([32, 12, 0])
                circle(screwHoleR, true);
    
            translate([-22, 22, 0])
                circle(screwHoleR, true);
            translate([-32, 12, 0])
                circle(screwHoleR, true);
    
            translate([22, -22, 0])
                circle(screwHoleR, true);
            translate([32, -12, 0])
                circle(screwHoleR, true);
        
            translate([-22, -22, 0])
                circle(screwHoleR, true);
            translate([-32, -12, 0])
                circle(screwHoleR, true);
        }
}

module motorMountHoles(){
    
    translate([motorMountScrewWidth/2, robotR-1.25*25.4-29.75, 0])
        circle(screwHoleR, true);
    translate([-motorMountScrewWidth/2, robotR-1.25*25.4-29.75, 0])
        circle(screwHoleR, true);
    
    translate([motorMountScrewWidth/2, robotR-1.25*25.4-84.75, 0])
        circle(screwHoleR, true);
    translate([-motorMountScrewWidth/2, robotR-1.25*25.4-84.75, 0])
        circle(screwHoleR, true);
}

module standoffHoles(){
    rotate(26){
        translate([0, robotR-10, 0])
            circle(screwHoleR, true);
    }
    rotate(-26){
        translate([0, robotR-10, 0])
            circle(screwHoleR, true);
    }
}


module wheelNotch(distOffCenter){
    translate([0, robotR-.625*25.4])
        square([sqrt(pow(wheelR,2) - pow(distOffCenter*25.4,2))*2 + 5, 1.25*25.4], true);
}

module rollerWheelNotch(distOffCenter){
    translate([52, robotR-ballR/2-4, 0])
        rotate(25)
            square([30, sqrt(pow(omniWheelR,2) - pow(distOffCenter*25.4,2))*2.25 + 2], true);
    
    translate([-52, robotR-ballR/2-4, 0])
        rotate(-25)
            square([30, sqrt(pow(omniWheelR,2) - pow(distOffCenter*25.4,2))*2.25 + 2], true);
}

module rollerWheelMountHoles(){
    translate([52, robotR-ballR/2-4, 0])
        rotate(25){
            translate([19.5, 13.5, 0])
                circle(1.5, true);
            translate([-19.5, 13.5, 0])
                circle(1.5, true);
            translate([19.5, -13.5, 0])
                circle(1.5, true);
            translate([-19.5, -13.5, 0])
                circle(1.5, true);
        }
    translate([-52, robotR-ballR/2-4, 0])
        rotate(-25){
            translate([19.5, 13.5, 0])
                circle(1.5, true);
            translate([-19.5, 13.5, 0])
                circle(1.5, true);
            translate([19.5, -13.5, 0])
                circle(1.5, true);
            translate([-19.5, -13.5, 0])
                circle(1.5, true);
        }
}

module motorControlWireHole(){
    translate([85, -70, 0])
        circle(15, true);
}

module batteryHole(){
    square([115, 47], true);
    
    translate([-80, -60, 0])
        circle(10.5, true);
    
    difference(){
        translate([-100, -20, 0])
            circle(10.5, true);
        
        translate([-85.4, -20, 0])
            square([10, 20], true);
        translate([-114.6, -20, 0])
            square([10, 20], true);
    }
}

if(part == "plate1" || part == "all"){
    color("blue")
    difference(){
        circle(robotR, true);
        
        translate([0, -147, 0])
            square([200, 10], true);
        
        translate([0, 70, 0])
            circle(screwHoleR, true);
        
        translate([0,robotR+ballR/2,0])
            circle(ballR, true);
        
        for(i = [-60 : 120 : 180]){
            rotate(i){
                wheelNotch(0.95);
                motorMountHoles();
                standoffHoles();
            }
        }
        
        rollerWheelNotch(0.65);
        rollerWheelMountHoles();
        motorControllerHoles();
    }
}

module terminalBlockHoles(){
    translate([2.25*25.4-7, -54, 0])
        circle(screwHoleR, true);
    translate([-2.25*25.4+7, -54, 0])
        circle(screwHoleR, true);
    translate([2.25*25.4-7, -112, 0])
        circle(screwHoleR, true);
    translate([-2.25*25.4+7, -112, 0])
        circle(screwHoleR, true);
    translate([0, -83, 0])
        square([4*25.4, 0.5*25.4], true);
}

module breadboardMountHoles(){
    translate([114, 44, 0])
        circle(screwHoleR, true);
    translate([129, 29, 0])
        circle(screwHoleR, true);
    translate([86, 44, 0])
        circle(screwHoleR, true);
    translate([71, 29, 0])
        circle(screwHoleR, true);
    translate([114, -44, 0])
        circle(screwHoleR, true);
    translate([129, -29, 0])
        circle(screwHoleR, true);
    translate([86, -44, 0])
        circle(screwHoleR, true);
    translate([71, -29, 0])
        circle(screwHoleR, true);
}

module motoGMountHoles(){
    translate([95, 0, 0]){
        translate([-26.5, 77.5, 0])
            circle(screwHoleR, true);
        translate([-41.5, 62.5, 0])
            circle(screwHoleR, true);
        translate([26.5, -77.5, 0])
            circle(screwHoleR, true);
        translate([41.5, -62.5, 0])
            circle(screwHoleR, true);
    }
}

module revPlateHoles(){
    translate([-10, -75, 0]){
        translate([64,44,0])
            circle(2.25);
        translate([-64,44,0])
            circle(2.25);
        translate([64,-44,0])
            circle(2.25);
        translate([-64,-44,0])
            circle(2.25);
    }
}

if(part == "plate2" || part == "all"){
    color("purple")
    translate([0, 0, 3*25.4])
    difference(){
        circle(robotR, true);
        
        translate([0, 70, 0])
            circle(screwHoleR, true);
        
        translate([0,robotR+ballR/2,0])
            circle(ballR+20, true);
        
        rotate(180)
            standoffHoles();
        rotate(86){
            translate([0, robotR-10, 0])
                circle(screwHoleR, true);
        }
        rotate(-86){
            translate([0, robotR-10, 0])
                circle(screwHoleR, true);
        }
        
        motorControlWireHole();
        batteryHole();
        terminalBlockHoles();
        breadboardMountHoles();
        revPlateHoles();
        motoGMountHoles();
    }
}

if(part == "all"){
    color("yellow")
    translate([0, robotR+ballR/2-7, ballR-31])
        sphere(ballR, true);

    translate([52, robotR-ballR/2-4, 0.65*25.4])
        rotate([-25, 90, 0])
            cylinder(25, 25, 25, true);
    translate([-52, robotR-ballR/2-4, 0.65*25.4])
        rotate([25, 90, 0])
            cylinder(25, 25, 25, true);  
}
